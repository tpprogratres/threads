
public class NoHilos {

	public static void main(String [] args){
		//este es un ejemplo de una ejecucion de un single threaded process
		//en este caso el ciclo se ejecuta en el orden que viene ,es decir, hasta que no termina una, no comienza otra
		//esto se conoce como monotarea
		
		for(int i = 0 ; i< 15; i++){
			System.out.println("hilo 1 executing");
		}
		System.out.println("fin hilo 1");
		for(int i = 0 ; i< 15; i++){
			System.out.println("hilo 2 executing");
		}
		System.out.println("fin hilo 2");
	}
}
