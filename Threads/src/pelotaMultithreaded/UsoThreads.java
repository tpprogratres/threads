package pelotaMultithreaded;

import java.awt.geom.*;

import javax.swing.*;

import java.util.*;
import java.awt.*;
import java.awt.event.*;

public class UsoThreads {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JFrame marco=new MarcoRebote();
		
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		marco.setVisible(true);

	}

}


class PelotaHilos implements Runnable{

	private Pelota pelota;
	private Component c;
	
	public PelotaHilos(Pelota unaPelota, Component d ){
		
		pelota= unaPelota;
		c = d;
		
	}
	@Override
	public void run() {
		//for (int i=1; i<=3000; i++){
		System.out.println("estado del thread : "+ Thread.currentThread().isInterrupted());	
		while(!Thread.currentThread().isInterrupted()){
			pelota.mueve_pelota(c.getBounds());
			
			c.paint(c.getGraphics());
			
			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				//e.printStackTrace();
				//System.out.println("hilo bloqueado, imposible su interrupcion");
				//debido al sleep no se puede interrumpir, ahora si si le pongo un interrupt dentro de este catch
				Thread.currentThread().interrupt();
			}
			
		}
		System.out.println("estado del thread : terminado "+ Thread.currentThread().isInterrupted());	
		
	}
	
	
}



//Movimiento de la pelota-----------------------------------------------------------------------------------------

class Pelota{
	
	// Mueve la pelota invirtiendo posici�n si choca con l�mites
	
	public void mueve_pelota(Rectangle2D limites){
		
		x+=dx;
		
		y+=dy;
		
		if(x<limites.getMinX()){
			
			x=limites.getMinX();
			
			dx=-dx;
		}
		
		if(x + TAMX>=limites.getMaxX()){
			
			x=limites.getMaxX() - TAMX;
			
			dx=-dx;
		}
		
		if(y<limites.getMinY()){
			
			y=limites.getMinY();
			
			dy=-dy;
		}
		
		if(y + TAMY>=limites.getMaxY()){
			
			y=limites.getMaxY()-TAMY;
			
			dy=-dy;
			
		}
		
	}
	
	//Forma de la pelota en su posici�n inicial
	
	public Ellipse2D getShape(){
		
		return new Ellipse2D.Double(x,y,TAMX,TAMY);
		
	}	
	
	private static final int TAMX=15;
	
	private static final int TAMY=15;
	
	private double x=0;
	
	private double y=0;
	
	private double dx=1;
	
	private double dy=1;
	
	
}

// L�mina que dibuja las pelotas----------------------------------------------------------------------


class LaminaPelota extends JPanel{
	
	//A�adimos pelota a la l�mina
	
	public void add(Pelota b){
		
		pelotas.add(b);
	}
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		
		Graphics2D g2=(Graphics2D)g;
		
		for(Pelota b: pelotas){
			
			g2.fill(b.getShape());
		}
		
	}
	
	private ArrayList<Pelota> pelotas=new ArrayList<Pelota>();
}


//Marco con l�mina y botones------------------------------------------------------------------------------

class MarcoRebote extends JFrame{
	
	public MarcoRebote(){
		
		setBounds(600,300,500,500);
		
		setTitle ("Rebotes");
		
		lamina=new LaminaPelota();
		
		add(lamina, BorderLayout.CENTER);
		
		JPanel laminaBotones=new JPanel();
		
		dale1 = new JButton("hilo1");
		dale1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				comienza_el_juego(e);
			}
		});
		laminaBotones.add(dale1);
		
		
		dale2 = new JButton("hilo2");
		dale2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				comienza_el_juego(e);
			}
		});
		laminaBotones.add(dale2);
		
		dale3 = new JButton("hilo3");
		dale3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				comienza_el_juego(e);
			}
		});
		laminaBotones.add(dale3);
		
		detener1 = new JButton("detenter1");
		detener1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				detener(e);
			}
		});
		laminaBotones.add(detener1);
		
		detener2 = new JButton("detenter2");
		detener2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				detener(e);
			}
		});
		laminaBotones.add(detener2);
		
		
		detener3 = new JButton("detenter3");
		detener3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				detener(e);
			}
		});
		laminaBotones.add(detener3);
		
		
		
		
		add(laminaBotones, BorderLayout.SOUTH);
	}
	
	
	//Ponemos botones
	
	public void ponerBoton(Container c, String titulo, ActionListener oyente){
		
		JButton boton=new JButton(titulo);
		
		c.add(boton);
		
		boton.addActionListener(oyente);
		
	}
	
	//A�ade pelota y la bota 1000 veces
	
	public void comienza_el_juego (ActionEvent e){
		
					
			Pelota pelota=new Pelota();
			
			lamina.add(pelota);
		
			//creo el runnable
			Runnable r = new PelotaHilos(pelota,lamina);
			
			if(e.getSource() == dale1){
				//creo un proceso
				t1 = new Thread (r);
				//inicio el hilo
				t1.start();
			}
			
			else if(e.getSource() == dale2){
				//creo un proceso
				t2 = new Thread (r);
				//inicio el hilo
				t2.start();
			}
			
			else if(e.getSource() == dale3){
				//creo un proceso
				t3 = new Thread (r);
				//inicio el hilo
				t3.start();
			}
			
			
		
		
	}
	
	public void detener(ActionEvent e){
		if(e.getSource() == detener1){
			t1.interrupt();
		}
		
		else if(e.getSource() == detener2){
			t2.interrupt();
		}
		
		else if(e.getSource() == detener3){
			t3.interrupt();
		}
		
	}
	
	private LaminaPelota lamina;
	private Thread t1, t2, t3;
	private JButton dale1, dale2, dale3, detener1,detener2,detener3;
	
}
