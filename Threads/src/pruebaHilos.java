
public class pruebaHilos {

	
	public static void main(String [] args){
		Hilo h1 = new Hilo ("hilo 1");
		Hilo h2 = new Hilo ("hilo 2");
		//por mas que empieze en este orden puede terminar primero el 2 
		//esto es porque existe un thread para cada uno y se ejecutan en simultaneo
		//quien termine antes depende de la capacidad de procesamiento del CPU
		
		h1.start();//metodo que inicia el thread
		h2.start();
	}
	
}
